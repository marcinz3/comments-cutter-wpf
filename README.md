# CommentsCutter BETA #

Program służący do wykonania zrzutów ekranu komentarzy pod wybranym filmem na YouTube, który ma przyśpieszyć przygotowywanie filmów typu Q&A.

### Prezentacja wideo ###

[https://youtu.be/B5tSiNsC-_o](https://youtu.be/B5tSiNsC-_o)

### Autor ###

* Marcin Zając
* Wszelkie prawa zastrzeżone