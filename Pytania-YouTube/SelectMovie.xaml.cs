﻿using Google.Apis.YouTube.v3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for SelectMovie.xaml
    /// </summary>
    public partial class SelectMovie : Window
    {
        public Movie Movie { get; private set; }

        public SelectMovie()
        {
            InitializeComponent();
            
            int downloadComments = (int)Properties.Settings.Default["DownloadComments"];
            if (downloadComments == 1)
            {
                All.IsChecked = true;
            }
            else if (downloadComments == 2)
            {
                StartFrom.IsChecked = true;
            }
            else if (downloadComments == 3)
            {
                RegularExp.IsChecked = true;
            }

            string _startFromText = (string)Properties.Settings.Default["StartFrom"];
            if (!String.IsNullOrEmpty(_startFromText))
            {
                StartFromText.Text = _startFromText;
            }

            string _regularExpText = (string)Properties.Settings.Default["RegularExp"];
            if (!String.IsNullOrEmpty(_regularExpText))
            {
                RegularExpText.Text = _regularExpText;
            }
        }

        private void SearchedTitle_KeyUp(object sender, KeyEventArgs e)
        {
            string text = this.SearchedTitle.Text;
            this.MoviesListView.Items.Clear();
            if (text.Length > 3)
            {
                YouTubeService yt = Application.Current.Properties["YouTubeService"] as YouTubeService;
                var searchListRequest = yt.Search.List("snippet");
                searchListRequest.Q = text;
                searchListRequest.Type = "video";
                searchListRequest.ChannelId = Properties.Settings.Default["ChannelId"] as string;
                var searchListResult = searchListRequest.Execute();
                foreach (var item in searchListResult.Items)
                {
                    this.MoviesListView.Items.Add(new Movie
                    {
                        MovieTitle = item.Snippet.Title,
                        MovieImg = item.Snippet.Thumbnails.Medium.Url,
                        MovieId = item.Id.VideoId
                    });
                }
            }
        }

        private void OpenNextWindow(Movie movie)
        {
            //Application.Current.Properties.Add("CurrentComments", "AllComments.dat");
            
            AllComments w = new AllComments(movie);
            w.Show();
            Close();
        }

        private void MoviesListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            //var item = ((FrameworkElement)e.OriginalSource).DataContext;

            VideoSelected();
        }

        private void OkButton_Click(object sender, RoutedEventArgs e)
        {
            VideoSelected();
        }

        private void VideoSelected()
        {
            var item = MoviesListView.SelectedItem;

            if (item != null)
            {
                Movie = (Movie)item;
                if (StartFrom.IsChecked == true)
                {
                    Movie.Regex = "^"+StartFromText.Text.Replace("[", "\\[").Replace("]", "\\]")
                        .Replace("(", "\\(").Replace(")", "\\)").Replace("{", "\\{").Replace("}", "\\}")
                        .Replace(".", "\\.")+"(.*)$";
                } 
                else if (RegularExp.IsChecked == true)
                {
                    Movie.Regex = RegularExpText.Text;
                }
                //MessageBox.Show(item.ToString() + " Item's Double Click handled! " + channel.ChannelId);
                //Application.Current.Properties.Add("VideoId", movie.MovieId);

                //OpenNextWindow(movie);
                if (All.IsChecked == true)
                {
                    Properties.Settings.Default["DownloadComments"] = 1;
                }
                else if (StartFrom.IsChecked == true)
                {
                    Properties.Settings.Default["DownloadComments"] = 2;
                }
                else if (RegularExp.IsChecked == true)
                {
                    Properties.Settings.Default["DownloadComments"] = 3;
                }
                Properties.Settings.Default["StartFrom"] = StartFromText.Text;
                Properties.Settings.Default["RegularExp"] = RegularExpText.Text;
                Properties.Settings.Default.Save();

                DialogResult = true;
                Close();
            }
            else
            {
                MessageBox.Show("Nie wybrałeś żadnego filmu!", "Wybór filmu", 
                    MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void CancelButton_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (MoviesListView.Items.Count > 0)
            {
                var columns = MoviesListGridView.Columns;
                double secondColWidth = MoviesListView.RenderSize.Width - columns.ElementAt(0).Width;
                //MessageBox.Show(columns.ElementAt(1).Width + " -> " + secondColWidth);
                columns.ElementAt(1).Width = secondColWidth;
            }
        }
    }
}
