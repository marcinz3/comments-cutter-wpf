﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for CommentsShow.xaml
    /// </summary>
    public partial class CommentsShow : Window
    {
        private QandA QandA;
        private List<CommentItem> Comments;
        private int _n;

        public CommentsShow(QandA QandA)
        {
            this.QandA = QandA;

            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fStream = File.OpenRead(QandA.FileName))
            {
                Comments = (List<CommentItem>)binFormat.Deserialize(fStream);
            }

            InitializeComponent();

            _n = QandA.CurrentComment;
            CommentsCount.Content = "/ " + Comments.Count;
            RefreshCommentView();
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            _n++;
            RefreshCommentView();
        }

        private void PrevButton_Click(object sender, RoutedEventArgs e)
        {
            _n--;
            RefreshCommentView();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^1-9]+"); //regex that matches disallowed text
            e.Handled = regex.IsMatch(e.Text);
        }

        private void nOfComments_KeyUp(object sender, KeyEventArgs e)
        {
            if (String.IsNullOrEmpty(nOfComments.Text))
                return;

            int n;
            try
            {
                n = int.Parse(nOfComments.Text);
                if (n < 1)
                {
                    n = 1;
                }
            }
            catch (Exception)
            {
                n = 1;
            }
            
            if (n > Comments.Count)
            {
                nOfComments.Text = Comments.Count.ToString();
            }
            //else
            {
                _n = n-1;
                RefreshCommentView();
            }
        }

        private void RefreshCommentView()
        {
            PrevButton.IsEnabled = true;
            NextButton.IsEnabled = true;

            if (_n < 1)
            {
                PrevButton.IsEnabled = false;
            }
            else if (_n >= Comments.Count - 1)
            {
                NextButton.IsEnabled = false;
            }

            double fromHeight = ActualHeight;

            var currentComment = Comments.ElementAt(_n);
            nOfComments.Text = (_n + 1).ToString();
            CommentImg.Source = new BitmapImage(new Uri(currentComment.AuthorImg));
            CommentAuthor.Content = currentComment.Author;
            CommentContent.Text = currentComment.Comment;
            
            this.SizeToContent = System.Windows.SizeToContent.Height;
            //this.BeginInit();
            //setting SizeToContent of window to Height get you the exact value of window height required to display completely
            
            //this.SizeToContent = System.Windows.SizeToContent.Height;
            //double height = this.ActualHeight;
            /*
            double height = fromHeight;
            if (CommentAuthor.Height + CommentContent.Height > CommentAuthorContent.Height)
                height += CommentAuthor.RenderSize.Height + CommentContent.Height - CommentAuthorContent.Height;
            MessageBox.Show(CommentContent.DesiredSize.Height+"");
            
            this.SizeToContent = System.Windows.SizeToContent.Manual;
            //run the animation code at backgroud for smoothness
            //this.Dispatcher.BeginInvoke(new Action(() =>
            {
                DoubleAnimation heightAnimation = new DoubleAnimation();
                heightAnimation.Duration = new Duration(new TimeSpan(0, 0, 2));
                heightAnimation.From = fromHeight;
                heightAnimation.To = height;
                heightAnimation.FillBehavior = FillBehavior.HoldEnd;
                this.BeginAnimation(Window.HeightProperty, heightAnimation);
            }//), null);
            //this.EndInit();
            */
        }

        private void CommentsShowWindow_Loaded(object sender, RoutedEventArgs e)
        {
            RefreshCommentView();
        }

        private void CommentsShowWindow_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            QandA.CurrentComment = _n;
        }
    }
}
