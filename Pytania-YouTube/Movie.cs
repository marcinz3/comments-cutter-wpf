﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pytania_YouTube
{
    [Serializable]
    public class Movie
    {
        public string MovieTitle { get; set; }

        public string MovieImg { get; set; }

        public string MovieId { get; set; }

        public string Regex { get; set; }
    }
}
