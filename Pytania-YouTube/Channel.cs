﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pytania_YouTube
{
    class Channel
    {
        public string ChannelName { get; set; }

        public string ChannelImg { get; set; }

        public string ChannelId { get; set; }
    }
}
