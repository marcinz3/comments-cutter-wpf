﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for SaveProgressDialog.xaml
    /// </summary>

    public partial class SaveProgressDialog : Window
    {
        private BitmapImage watermarkBitmapImage;
        private const int WATERMARK_WIDTH = 134;
        private const int WATERMARK_HEIGHT = 34;

        class SaveProgressDialogDetails
        {
            public string FilePath;
            public string FileExtension;
            public BitmapEncoder bitmapEncoder;
            public List<CommentItem> comments;
        }
        private SaveProgressDialogDetails _details;

        public SaveProgressDialog(List<CommentItem> comments, string FilePath, string FileExtension, BitmapEncoder bitmapEncoder)
        {
            _details = new SaveProgressDialogDetails();
            _details.FilePath = FilePath;
            _details.FileExtension = FileExtension;
            _details.bitmapEncoder = bitmapEncoder;
            _details.comments = comments;

            watermarkBitmapImage = new BitmapImage(new Uri("pack://application:,,,/Pytania-YouTube;component/Images/Coment-Cuter-logo.png", UriKind.RelativeOrAbsolute));

            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //_saveProgressDialog.ReportProgress(0, null, string.Format(System.Globalization.CultureInfo.CurrentCulture, "Pracuję: {0}%", 0));
            progressbar.Value = 0;
            percentageLabel.Content = progressbar.Value + "%";

            //_saveWorker.RunWorkerAsync(_details);
            TaskScheduler uiContext = TaskScheduler.FromCurrentSynchronizationContext();
            Task.Run(async () =>
            {
                var details = _details;
                int countOfItems = details.comments.Count;
                int i = 1;

                foreach (CommentItem item in details.comments)
                {
                    await Task.Factory.StartNew(() =>
                    {
                        var formattedText = new FormattedText(
                            item.Comment,
                            CultureInfo.CurrentUICulture,
                            FlowDirection.LeftToRight,
                            new Typeface("Verdana"),//Typeface(new FontFamily("Segoe UI"), textBlock.FontStyle, textBlock.FontWeight, textBlock.FontStretch),
                            12,
                            Brushes.Black
                        );

                        double height = formattedText.Height;
                        double width = formattedText.Width + 10;

                        var formattedAuthorText = new FormattedText(
                            item.Author,
                            CultureInfo.CurrentUICulture,
                            FlowDirection.LeftToRight,
                            new Typeface(new FontFamily("Verdana"), FontStyles.Normal, FontWeight.FromOpenTypeWeight(500), System.Windows.FontStretches.Normal),
                            12,
                            new SolidColorBrush(Color.FromRgb(18, 142, 233))
                        );

                        if (formattedAuthorText.Width + WATERMARK_WIDTH > width)
                        {
                            width = formattedAuthorText.Width + WATERMARK_WIDTH + 10;
                        }
                        height += formattedAuthorText.Height + 20;

                        width += 48 + 20;
                        if (height < 68)
                            height = 68;
                    
                        var visual = new DrawingVisual();
                        DrawingContext drawingContext = visual.RenderOpen();

                        if (details.bitmapEncoder.CodecInfo.FriendlyName == "JPEG Encoder")
                        {
                            var whiteBrush = new SolidColorBrush(Colors.White);
                            drawingContext.DrawRectangle(whiteBrush, new Pen(whiteBrush, 1), new Rect(0, 0, width, height));
                        }
                        drawingContext.DrawImage(item.bitmapImage/*b(BitmapImage)sender2*/, new Rect(10, 10, 48, 48));

                        drawingContext.DrawImage(watermarkBitmapImage, new Rect(formattedAuthorText.Width + 68, 0, WATERMARK_WIDTH, WATERMARK_HEIGHT));

                        drawingContext.DrawText(formattedAuthorText, new Point(68, 10));
                        drawingContext.DrawText(formattedText, new Point(68, 15 + formattedAuthorText.Height));
                        
                        drawingContext.Close();

                        RenderTargetBitmap bitmap = new RenderTargetBitmap((int)width, (int)height, 96, 96, PixelFormats.Pbgra32);
                        bitmap.Render(visual);

                        BitmapFrame frame = BitmapFrame.Create(bitmap);

                        Type bitmapType = details.bitmapEncoder.GetType();
                        details.bitmapEncoder = (BitmapEncoder)Activator.CreateInstance(bitmapType);
                        details.bitmapEncoder.Frames.Add(frame);

                        using (var stream = File.Create(details.FilePath + @"\" + i + details.FileExtension))
                        {
                            details.bitmapEncoder.Save(stream);
                        }

                        progressbar.Value = i * 100 / countOfItems;
                        percentageLabel.Content = progressbar.Value + "%";
                        //System.Threading.Thread.Sleep(100);
                    }, CancellationToken.None, TaskCreationOptions.None, uiContext);
                    
                    i++;
                }
            });
        }

        private void progressbar_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (progressbar.Value == 100)
            {
                Process.Start(_details.FilePath);
                Close();
            }
        }
    }
}
