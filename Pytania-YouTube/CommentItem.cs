﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace Pytania_YouTube
{
    [Serializable]
    public class CommentItem
    {
        public CommentItem()
        {
            //Checked = false;
        }

        //public bool Checked { get; set; }

        public string Author { get; set; }

        public string AuthorImg { get; set; }
        //public ImageSource AuthorImg { get; set; }

        [NonSerialized]
        private BitmapImage _bitmapImage;

        public BitmapImage bitmapImage
        {
            get
            {
                return _bitmapImage;
            }
            set
            {
                _bitmapImage = value;
            }
        }

        public long LikeCount { get; set; }

        public string Comment { get; set; }

        public string CommentWithoutN { get; set; }
    }
}
