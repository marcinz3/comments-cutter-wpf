﻿using Google.Apis.Services;
using Google.Apis.YouTube.v3;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;


namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            YouTubeService yt = new YouTubeService(new BaseClientService.Initializer() { ApiKey = "AIzaSyAgiysJG7JLjrGz3X8Vsaxk_jaBkgTNbXU" });
            Application.Current.Properties.Add("YouTubeService", yt);
        }

        private void Application_Exit(object sender, ExitEventArgs e)
        {
            
        }
    }
}
