﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pytania_YouTube
{
    [Serializable]
    public class QandA
    {
        public string Name { get; set; }

        public string FileName { get; set; }

        public Movie Movie { get; set; }

        public DateTime InsertionDate { get; set; }

        public int CurrentComment { get; set; }
    }
}
