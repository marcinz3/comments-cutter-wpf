﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Google.Apis.YouTube.v3;
using Google.Apis.Services;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for ChannelChoose.xaml
    /// </summary>
    public partial class ChannelChoose : Window
    {
        private bool isModal;

        public ChannelChoose()
        {
            this.isModal = false;
            Init();
        }

        public ChannelChoose(bool isModal)
        {
            this.isModal = isModal;
            Init();
        }

        private void Init()
        {
            string channelId = (string)Properties.Settings.Default["ChannelId"];
            if (!String.IsNullOrEmpty(channelId) && !isModal)
            {
                OpenNextWindow();
            }

            InitializeComponent();
            TextEntered();
        }

        private void OpenNextWindow()
        {
            if (!isModal)  //if not showdioalog
            {
                MainWindow w = new MainWindow();
                w.Show();
            }
            
            Close();
        }

        private void ListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext;
            if (item != null)
            {
                Channel channel = (Channel)item;
                //MessageBox.Show(item.ToString() + " Item's Double Click handled! " + channel.ChannelId);
                Properties.Settings.Default["ChannelId"] = channel.ChannelId;
                Properties.Settings.Default.Save();

                OpenNextWindow();
            }
        }

        private void ChannelNameTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            TextEntered();
        }

        private void TextEntered()
        {
            string text = this.ChannelNameTextBox.Text;
            this.ChannelsListView.Items.Clear();
            if (text.Length > 3)
            {
                YouTubeService yt = Application.Current.Properties["YouTubeService"] as YouTubeService;
                var searchListRequest = yt.Search.List("snippet");
                searchListRequest.Q = text;
                searchListRequest.Type = "channel";
                var searchListResult = searchListRequest.Execute();
                foreach (var item in searchListResult.Items)
                {
                    this.ChannelsListView.Items.Add(new Channel
                    {
                        ChannelName = item.Snippet.Title,
                        ChannelImg = item.Snippet.Thumbnails.Medium.Url,
                        ChannelId = item.Snippet.ChannelId
                    });
                }
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            if (ChannelsListView.Items.Count > 0)
            {
                var columns = ChannelsListGridView.Columns;
                double secondColWidth = ChannelsListView.RenderSize.Width - 50;
                //MessageBox.Show(columns.ElementAt(1).Width + " -> " + secondColWidth);
                columns.ElementAt(1).Width = secondColWidth;
            }
        }
    }
}
