﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using Google.Apis.YouTube.v3;
using Google.Apis.Services;
using System.Threading;
using System.Windows.Threading;
using System.ComponentModel;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text.RegularExpressions;
using System.Globalization;
using Ookii.Dialogs.Wpf;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for AllComments.xaml
    /// </summary>
    public partial class AllComments : Window
    {
        private readonly BackgroundWorker AddItemsWorker = new BackgroundWorker();
        private bool isNewQandA;
        public QandA QandA {get; private set;}

        public AllComments(Movie movie)
        {
            Init();
            isNewQandA = true;
            QandA = new QandA();
            QandA.Movie = movie;
        }

        public AllComments(QandA qAndA)
        {
            Init();
            isNewQandA = false;
            this.QandA = qAndA;
            QandA_Name.Text = QandA.Name;
        }

        private void Init()
        {
            InitializeComponent();
            AddItemsWorker.DoWork += AddItems_DoWork;
            AddItemsWorker.RunWorkerCompleted += AddItems_RunWorkerCompleted;
            AddItemsWorker.ProgressChanged += AddItems_ProgressChanged;
            AddItemsWorker.WorkerReportsProgress = true;
        }

        private void AddItems_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            progressLabel.Content = e.ProgressPercentage + "%";
        }

        private void AddItems_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (var item in e.Result as List<CommentItem>)
            {
                item.bitmapImage = new BitmapImage();
                item.bitmapImage.BeginInit();
                item.bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
                item.bitmapImage.UriSource = new Uri(item.AuthorImg, UriKind.RelativeOrAbsolute);
                item.bitmapImage.EndInit();
                commentsListView.Items.Add(item);
            }
            PreloaderImg.Visibility = System.Windows.Visibility.Hidden;
            progressLabel.Visibility = System.Windows.Visibility.Hidden;
        }

        private void AddItems_DoWork(object sender, DoWorkEventArgs e)
        {
            if (isNewQandA)
            {
                e.Result = DownloadComments();
            }
            else
            {
                e.Result = ReadComments((string)e.Argument);
            }
        }

        private List<CommentItem> ReadComments(string FileName)
        {
            BinaryFormatter binFormat = new BinaryFormatter();

            using (Stream fStream = File.OpenRead(FileName))
            {
                return (List<CommentItem>)binFormat.Deserialize(fStream);
            }
        }

        private List<CommentItem> DownloadComments()
        {
            //MessageBox.Show(Properties.Settings.Default["ChannelId"].ToString());
            YouTubeService yt = Application.Current.Properties["YouTubeService"] as YouTubeService;

            List<CommentItem> items = new List<CommentItem>();
            int i = 0;
            string nextPageToken = "";
            
            var videoInfo = yt.Videos.List("statistics");
            videoInfo.Id = QandA.Movie.MovieId;
            var videoInfoResult = videoInfo.Execute();
            int commentsCount = Convert.ToInt32(videoInfoResult.Items.ElementAt(0).Statistics.CommentCount);
            Console.WriteLine("Liczba komentarzy: " + commentsCount);
            int downloadedCommentsNumber = 0;
            /*ulong maxResults = 0;

            for (ulong i = 0; maxResults % 100 == 0; i += 100)*/
            do
            {
                /*if (commentsCount > 100)
                {
                    commentsCount -= 100;
                    maxResults = i + 100;
                }
                else
                    maxResults = i + commentsCount;*/

                var commentsList = yt.CommentThreads.List("id,snippet");
                //commentsList.VideoId = "GdN5W2KTq4g";
                commentsList.VideoId = QandA.Movie.MovieId;
                commentsList.MaxResults = 100;
                commentsList.PageToken = nextPageToken;
                commentsList.TextFormat = CommentThreadsResource.ListRequest.TextFormatEnum.PlainText;
                var commentsListResult = commentsList.Execute();
                nextPageToken = commentsListResult.NextPageToken;
                Console.WriteLine("total: " + commentsListResult.PageInfo.TotalResults);
                
                bool isFiltred = false;
                Regex reg = null;
                if (!String.IsNullOrEmpty(QandA.Movie.Regex))
                {
                    reg = new Regex(QandA.Movie.Regex,
                        RegexOptions.IgnoreCase | RegexOptions.Multiline);
                    isFiltred = true;
                }

                foreach (var item in commentsListResult.Items)
                {
                    var commentSnippet = item.Snippet.TopLevelComment.Snippet;
                    string content = commentSnippet.TextDisplay;
                    //Console.WriteLine(commentSnippet.LikeCount+" "+content);
                    if (!isFiltred || (isFiltred && reg.IsMatch(content) == true))
                    {
                        items.Add(new CommentItem
                        {
                            Author = commentSnippet.AuthorDisplayName,
                            AuthorImg = commentSnippet.AuthorProfileImageUrl,
                            LikeCount = (long)commentSnippet.LikeCount,
                            Comment = content,
                            CommentWithoutN = content.Replace("\n", " ").Replace("  ", " ")
                        });
                    }
                    //Console.WriteLine("Postep: " + (int)((downloadedCommentsNumber+1) * 100 / commentsCount)+" - "+downloadedCommentsNumber+1);
                    AddItemsWorker.ReportProgress((int)((++downloadedCommentsNumber)*100/commentsCount));

                    var repliesComments = yt.Comments.List("snippet");
                    repliesComments.ParentId = item.Id;
                    repliesComments.MaxResults = 100;
                    repliesComments.TextFormat = CommentsResource.ListRequest.TextFormatEnum.PlainText;
                    var repliesCommentsResult = repliesComments.Execute();
                    foreach (var commentReply in repliesCommentsResult.Items)
                    {
                        content = commentReply.Snippet.TextDisplay;
                        //Console.WriteLine(" -> Reply " + commentReply.Snippet.LikeCount + " " + content);
                        if (!isFiltred || (isFiltred && reg.IsMatch(content) == true))
                        {
                            items.Add(new CommentItem
                            {
                                Author = commentReply.Snippet.AuthorDisplayName,
                                AuthorImg = commentReply.Snippet.AuthorProfileImageUrl,
                                LikeCount = (long)commentReply.Snippet.LikeCount,
                                Comment = content,
                                CommentWithoutN = content.Replace("\n", " ").Replace("  ", " ")
                            });
                        }
                        AddItemsWorker.ReportProgress((int)((++downloadedCommentsNumber) * 100 / commentsCount));
                    }
                }
                //if (i == 5) break;
                i++;
            } while (!string.IsNullOrEmpty(nextPageToken));
            Console.WriteLine(downloadedCommentsNumber + " z " + commentsCount);

            return items;
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            AddItemsWorker.RunWorkerAsync(QandA.FileName);
        }

        private void SelectAll_Click(object sender, RoutedEventArgs e)
        {
            commentsListView.SelectAll();
        }

        private void UnselectAll_Click(object sender, RoutedEventArgs e)
        {
            commentsListView.UnselectAll();
        }

        private void DeleteButton_Click(object sender, RoutedEventArgs e)
        {
            var items = commentsListView.SelectedItems;
            foreach (var item in items)
            {
                System.Windows.Threading.Dispatcher.CurrentDispatcher.BeginInvoke((Action)(() =>
                {
                    commentsListView.Items.Remove(item);
                }));
            }
        }

        private void NextButton_Click(object sender, RoutedEventArgs e)
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            var items = new List<CommentItem>();
            var selectedItems = commentsListView.Items;//commentsListView.SelectedItems;
            foreach (var item in selectedItems)
            {
                items.Add((CommentItem)item);
            }

            if (QandA.Name != QandA_Name.Text && File.Exists(QandA.FileName))
            {
                File.Delete(QandA.FileName);
            }

            QandA.Name = QandA_Name.Text;
            QandA.FileName = QandA.Name.Replace(" ", "-") + ".dat";
            if (isNewQandA)
            {
                if (String.IsNullOrEmpty(QandA_Name.Text))
                {
                    MessageBox.Show("Podaj nazwę tego QandA!", "Puste pole", 
                        MessageBoxButton.OK, MessageBoxImage.Error);
                    return;
                }
                QandA.InsertionDate = DateTime.Now;
                try
                {
                    using (Stream fStream = new FileStream(QandA.FileName, 
                        FileMode.CreateNew, FileAccess.Write, FileShare.None))
                    {
                        binFormat.Serialize(fStream, items);
                    }
                    DialogResult = true;
                    Close();
                }
                catch (IOException)
                {
                    MessageBox.Show("Już istnieje QandA o takiej nazwie!");
                }
                return;
            }

            using(Stream fStream = new FileStream(QandA.FileName, 
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, items);
            }
            
            DialogResult = true;
            Close();
        }

        private void jpg_Click(object sender, RoutedEventArgs e)
        {
            /*var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            using (var dlg = new System.Windows.Forms.FolderBrowserDialog())
            {
                dlg.Description = "Wybierz folder, w którym zostaną zapisane pliki z komentarzami";
                dlg.SelectedPath = "";
                dlg.ShowNewFolderButton = true;
                System.Windows.Forms.DialogResult dlgResult = dlg.ShowDialog();
                if (dlgResult == System.Windows.Forms.DialogResult.OK)
                {
                    MessageBox.Show(dlg.SelectedPath);
                    //BindingExpression be = GetBindingExpression(TextProperty);
                    //if (be != null)
                    //    be.UpdateSource();
                }
                else
                    return;
            }*/

            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
            dialog.Description = "Wybierz folder";
            dialog.UseDescriptionForTitle = true; // This applies to the Vista style dialog only, not the old dialog.
            //if (!VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
                //MessageBox.Show(this, "Because you are not using Windows Vista or later, the regular folder browser dialog will be used. Please use Windows Vista to see the new dialog.", "Sample folder browser dialog");
            if ((bool)dialog.ShowDialog(this))
            {
                var items = new List<CommentItem>();
                var selectedItems = commentsListView.Items;//commentsListView.SelectedItems;
                foreach (var item in selectedItems)
                {
                    items.Add((CommentItem)item);
                }
                SaveProgressDialog w = new SaveProgressDialog(items, dialog.SelectedPath, ".jpg", new JpegBitmapEncoder());
                w.ShowDialog();
            }
            
            //if (!_saveProgressDialog.IsBusy)
            //_saveProgressDialog.Show();
            
        }

        private void png_Click(object sender, RoutedEventArgs e)
        {
            VistaFolderBrowserDialog dialog = new VistaFolderBrowserDialog();
            dialog.Description = "Wybierz folder";
            dialog.UseDescriptionForTitle = true; // This applies to the Vista style dialog only, not the old dialog.
            //if (!VistaFolderBrowserDialog.IsVistaFolderDialogSupported)
            //MessageBox.Show(this, "Because you are not using Windows Vista or later, the regular folder browser dialog will be used. Please use Windows Vista to see the new dialog.", "Sample folder browser dialog");
            if ((bool)dialog.ShowDialog(this))
            {
                var items = new List<CommentItem>();
                var selectedItems = commentsListView.Items;//commentsListView.SelectedItems;
                foreach (var item in selectedItems)
                {
                    items.Add((CommentItem)item);
                }
                SaveProgressDialog w = new SaveProgressDialog(items, dialog.SelectedPath, ".png", new PngBitmapEncoder());
                w.ShowDialog();
            }
        }
    }
}
