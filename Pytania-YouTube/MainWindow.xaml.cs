﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using Google.Apis.YouTube.v3;
using Google.Apis.Services;
using System.Threading;
using System.Windows.Threading;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System.ComponentModel;

namespace Pytania_YouTube
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<QandA> QandAs;
        private CollectionView collectionView;

        public MainWindow()
        {
            MessageBox.Show("Uwaga!\nAplikacja jest w bardzo wczesnej wersji przez co może działać niestabilnie, zawieszać się oraz samoczynnie wyłączać, choć zdaża się to żadko, gdyż aplikację cały czas dopracowujemy.\n"
                + "Do tego pobieranie komentarzy trwa długo.\nSpokojnie w wersji finalnej będzie dużo lepiej.\n"
                + "Jeżeli natrafisz na jakiś błąd to prosimy o informacje.", 
                "Wersja beta!", MessageBoxButton.OK, MessageBoxImage.Information);
            InitializeComponent();
            
            BinaryFormatter binFormat = new BinaryFormatter();
            try
            {
                using (Stream fStream = File.OpenRead("QandAs.dat"))
                {
                    QandAs = (List<QandA>)binFormat.Deserialize(fStream);
                }
            }
            catch (FileNotFoundException)
            {
                QandAs = new List<QandA>();
            }
            catch (UnauthorizedAccessException)
            {
                MessageBox.Show("Brak uprawnień do wczytania pliku z zapisanymi QandA!");
                QandAs = new List<QandA>();
            }
            catch (Exception)
            {
                MessageBox.Show("Nieoczekiwany błąd otwarcia pliku!");
                QandAs = new List<QandA>();
            }

            QandAsListView.ItemsSource = QandAs;
            collectionView = (CollectionView)CollectionViewSource.GetDefaultView(QandAsListView.ItemsSource);
        }

        private void SaveQandAs()
        {
            BinaryFormatter binFormat = new BinaryFormatter();
            using (Stream fStream = new FileStream("QandAs.dat",
                FileMode.Create, FileAccess.Write, FileShare.None))
            {
                binFormat.Serialize(fStream, QandAs);
            }
        }

        private void NewQandAButton_Click(object sender, RoutedEventArgs e)
        {
            SelectMovie w = new SelectMovie();
            if (w.ShowDialog() == true)
            {
                AllComments allComments = new AllComments(w.Movie);
                if (allComments.ShowDialog() == true)
                {
                    QandAs.Add(allComments.QandA);
                    collectionView.Refresh();
                    SaveQandAs();

                    CommentsShow csw = new CommentsShow(allComments.QandA);
                    csw.Show();
                }
            }
        }

        private void QandAsListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext;
            
            if (item != null && item is QandA)
            {
                AllComments allComments = new AllComments((QandA)item);
                //allComments.Show();
                if (allComments.ShowDialog() == true)
                {
                    collectionView.Refresh();
                    SaveQandAs();

                    CommentsShow csw = new CommentsShow(allComments.QandA);
                    csw.Show();
                }
            }
        }

        private void EditQandA_Click(object sender, RoutedEventArgs e)
        {
            var item = ((FrameworkElement)e.OriginalSource).DataContext;

            if (item != null)
            {
                AllComments allComments = new AllComments((QandA)item);
                if (allComments.ShowDialog() == true)
                {
                    collectionView.Refresh();
                    SaveQandAs();
                }
            }
        }

        private void DeleteQandA_Click(object sender, RoutedEventArgs e)
        {
            var item = (QandA)((FrameworkElement)e.OriginalSource).DataContext;

            if (item != null && 
                MessageBox.Show("Czy na pewno chcesz usunąć ten QandA?", item.Name, 
                MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
            {
                QandAs.Remove(item);
                collectionView.Refresh();
                //QandAsListView.Items.Refresh();
                SaveQandAs();
                if (File.Exists(item.FileName))
                {
                    File.Delete(item.FileName);
                }
            }
        }

        private void OptionButton_Click(object sender, RoutedEventArgs e)
        {
            ChannelChoose w = new ChannelChoose(true);
            w.ShowDialog();
        }

        private void GridViewColumnHeader_Click(object sender, RoutedEventArgs e)
        {
            GridViewColumnHeader header = (sender as GridViewColumnHeader);
            string columnNameToSort = header.DataContext as string;
            ListSortDirection howToSort = ListSortDirection.Ascending;

            if (collectionView.SortDescriptions.Any())
            {
                SortDescription item = collectionView.SortDescriptions.ElementAt(0);

                if (columnNameToSort == item.PropertyName.ToString() && item.Direction == ListSortDirection.Ascending)
                    howToSort = ListSortDirection.Descending;
            }

            collectionView.SortDescriptions.Clear();
            collectionView.SortDescriptions.Add(new SortDescription(columnNameToSort, howToSort));
        }
    }
}
